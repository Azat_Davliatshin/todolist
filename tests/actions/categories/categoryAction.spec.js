import {categoryAction} from '../../../app/actions';

expect.extend({
    toBeActionObject(recieved, expected) {
        let pass;
        let message;
        try {
            message = `Testing object is not defined.\n Message from spec: \n`;
            //new action should be defined
            expect(recieved).toBeDefined();
            message = `Testing object is not a object.\n Message from spec: \n`;
            //it should be an object
            expect(typeof recieved).toBe('object');
            message = `Testing object doesn't have required property 'type'.\n Message from spec: \n`;
            //neccessary condition - haave to be defined 'type' property
            expect(recieved.type).toBeDefined();
            message = `Testing object is not equal to passed value.\n Message from spec: \n`;
            //check for equality
            expect(recieved).toEqual(expected);
            pass = true;
        } catch (e) {
            pass = false;
            message += e.message;
        } finally {
            return {message, pass};
        }
    }
});
describe('category actions test for ', () => {
    describe('categories ', () => {
        let categoryId = '0';
        //just to practice
        beforeEach(() => {
            categoryId = `${parseInt(categoryId) + 1}`;
        });
        describe('should add category ', () => {
            const expectedAction = {
                type: categoryAction.ADD_CATEGORY,
                text: ''
            };
            it('without explicit text', () => {
                const addedCategoryAction = categoryAction.addCategory();
                expectedAction.text = 'Category';
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
            it('with explicit text', () => {
                const categoryName = expectedAction.text = 'react application';
                const addedCategoryAction = categoryAction.addCategory(categoryName);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
        describe('should edit category ', () => {
            it('with explicit text and category id', () => {
                const text = 'gym';
                const expectedAction = {
                    type: categoryAction.EDIT_CATEGORY,
                    text,
                    categoryId
                };
                const addedCategoryAction = categoryAction.editCategory(text, categoryId);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
        describe('should delete category ', () => {
            it('with category id', () => {
                const expectedAction = {
                    type: categoryAction.DELETE_CATEGORY,
                    categoryId
                };
                const addedCategoryAction = categoryAction.deleteCategory(categoryId);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
        describe('should extend category ', () => {
            it('with category id', () => {
                const expectedAction = {
                    type: categoryAction.EXTEND_CATEGORY,
                    categoryId
                };
                const addedCategoryAction = categoryAction.extendCategory(categoryId);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
        describe('should expand category ', () => {
            it('with category id', () => {
                const expectedAction = {
                    type: categoryAction.EXPAND_CATEGORY,
                    categoryId
                };
                const addedCategoryAction = categoryAction.expandCategory(categoryId);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
        describe('should make invisible category ', () => {
            it('with category id', () => {
                const expectedAction = {
                    type: categoryAction.MAKE_INVISIBLE,
                    categoryId
                };
                const addedCategoryAction = categoryAction.makeInvisible(categoryId);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
        describe('should toogle category ', () => {
            it('with category id and checking value', () => {
                const expectedAction = {
                    type: categoryAction.TOOGLE_CATEGORY,
                    categoryId,
                    checked: true
                };
                const addedCategoryAction = categoryAction.toogleCategory(categoryId, true);
                expect(addedCategoryAction).toBeActionObject(expectedAction);
            });
        });
    });

    describe('tasks ', () => {
        let categoryId = '0';
        beforeEach(() => {
            categoryId = `${parseInt(categoryId) + 1}`
        });
        describe('should add task with category id and ', () => {
            it('without explicit text', () => {
                const expectedAction = {
                    type: categoryAction.ADD_TASK,
                    categoryId,
                    text: 'To Do Item',
                    task: undefined
                };
                const addedTaskAction = categoryAction.addTask(undefined, categoryId);
                expect(addedTaskAction).toBeActionObject(expectedAction);
            });
            it('with explicit text', () => {
                const newTaskName = 'need to do';
                const expectedAction = {
                    type: categoryAction.ADD_TASK,
                    categoryId,
                    text: newTaskName,
                    task: undefined
                };
                const addedTaskAction = categoryAction.addTask(newTaskName, categoryId);
                expect(addedTaskAction).toBeActionObject(expectedAction);
            });
        });
        describe('should toogle task ', () => {
            it('with category id, task id and checked', () => {
                const addCategoryAction = categoryAction.toogleTask('0', "0", true);
                expect(typeof addCategoryAction).toBe('object');
                expect(addCategoryAction.type).toBeDefined();
                expect(addCategoryAction.taskId).toBeDefined();
                expect(addCategoryAction.categoryId).toBeDefined();
                expect(addCategoryAction.checked).toBeDefined();

                expect(typeof addCategoryAction.type).toBe('string');
                expect(addCategoryAction.type).toBe(categoryAction.TOOGLE_TASK);

                expect(typeof addCategoryAction.categoryId).toBe('string');
                expect(addCategoryAction.categoryId).toBe('0');

                expect(typeof addCategoryAction.taskId).toBe('string');
                expect(addCategoryAction.taskId).toBe('0');

                expect(typeof addCategoryAction.checked).toBe('boolean');
                expect(addCategoryAction.checked).toBeTruthy();
            });
        });
        describe('should delete task ', () => {
            it('with category id and task id', () => {
                const addCategoryAction = categoryAction.deleteTask('0', "0");
                expect(typeof addCategoryAction).toBe('object');
                expect(addCategoryAction.type).toBeDefined();
                expect(addCategoryAction.taskId).toBeDefined();
                expect(addCategoryAction.categoryId).toBeDefined();

                expect(typeof addCategoryAction.type).toBe('string');
                expect(addCategoryAction.type).toBe(categoryAction.DELETE_TASK);

                expect(typeof addCategoryAction.categoryId).toBe('string');
                expect(addCategoryAction.categoryId).toBe('0');

                expect(typeof addCategoryAction.taskId).toBe('string');
                expect(addCategoryAction.taskId).toBe('0');

            });
        });

        describe('should rename task ', () => {
            it('with category id and task id and text', () => {
                const addCategoryAction = categoryAction.renameTask('0', "0", 'New name');
                expect(typeof addCategoryAction).toBe('object');
                expect(addCategoryAction.type).toBeDefined();
                expect(addCategoryAction.taskId).toBeDefined();
                expect(addCategoryAction.categoryId).toBeDefined();
                expect(addCategoryAction.text).toBeDefined();

                expect(typeof addCategoryAction.type).toBe('string');
                expect(addCategoryAction.type).toBe(categoryAction.RENAME_TASK);

                expect(typeof addCategoryAction.categoryId).toBe('string');
                expect(addCategoryAction.categoryId).toBe('0');

                expect(typeof addCategoryAction.taskId).toBe('string');
                expect(addCategoryAction.taskId).toBe('0');

                expect(typeof addCategoryAction.text).toBe('string');
                expect(addCategoryAction.text).toBe('New name');
            });
        });
        describe('should add task description ', () => {
            it('with category id and task id and text', () => {
                const addCategoryAction = categoryAction.addTaskDescription('0', "0", 'some description');
                expect(typeof addCategoryAction).toBe('object');
                expect(addCategoryAction.type).toBeDefined();
                expect(addCategoryAction.taskId).toBeDefined();
                expect(addCategoryAction.categoryId).toBeDefined();
                expect(addCategoryAction.text).toBeDefined();

                expect(typeof addCategoryAction.type).toBe('string');
                expect(addCategoryAction.type).toBe(categoryAction.ADD_TASK_DESCRIPTION);

                expect(typeof addCategoryAction.categoryId).toBe('string');
                expect(addCategoryAction.categoryId).toBe('0');

                expect(typeof addCategoryAction.taskId).toBe('string');
                expect(addCategoryAction.taskId).toBe('0');

                expect(typeof addCategoryAction.text).toBe('string');
                expect(addCategoryAction.text).toBe('some description');
            });
        });
    });

    describe('history actions ', () => {
        it('should add to history', () => {
            const addCategoryAction = categoryAction.addToHistory();
            expect(typeof addCategoryAction).toBe('object');
            expect(addCategoryAction.type).toBeDefined();

            expect(typeof addCategoryAction.type).toBe('string');
            expect(addCategoryAction.type).toBe(categoryAction.ADD_TO_HISTORY);
        });
        it('should undo', () => {
            const addCategoryAction = categoryAction.undo();
            expect(typeof addCategoryAction).toBe('object');
            expect(addCategoryAction.type).toBeDefined();

            expect(typeof addCategoryAction.type).toBe('string');
            expect(addCategoryAction.type).toBe(categoryAction.UNDO);
        });
        it('should redo', () => {
            const addCategoryAction = categoryAction.redo();
            expect(typeof addCategoryAction).toBe('object');
            expect(addCategoryAction.type).toBeDefined();

            expect(typeof addCategoryAction.type).toBe('string');
            expect(addCategoryAction.type).toBe(categoryAction.REDO);
        });
    })
});
