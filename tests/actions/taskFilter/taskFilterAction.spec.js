import {taskFilterAction} from '../../../app/actions';

describe('category actions test for tasks filter', () => {
  it('should keep inital value',()=>{
    const action = taskFilterAction.setFilterText();
    expect(action.type).toBeDefined();
    expect(action.text).toBeDefined();

    expect(typeof action.type).toBe('string');
    expect(typeof action.text).toBe('string');

    expect(action.type).toBe(taskFilterAction.SET_FILTER_TEXT);
    expect(action.text).toBe('');
  });
  it('should toogle value',()=>{
    const action = taskFilterAction.setFilterText('find some task');
    expect(action.type).toBeDefined();
    expect(action.text).toBeDefined();

    expect(typeof action.type).toBe('string');
    expect(typeof action.text).toBe('string');

    expect(action.type).toBe(taskFilterAction.SET_FILTER_TEXT);
    expect(action.text).toBe('find some task');
  });
});
