import {visibilityFilterAction} from '../../../app/actions';

describe('category actions test for tasks visibility filter', () => {
  it('should keep inital value',()=>{
    const action = visibilityFilterAction.setVisibilityFilter();
    expect(action.type).toBeDefined();
    expect(action.filter).toBeDefined();

    expect(typeof action.type).toBe('string');
    expect(typeof action.filter).toBe('string');

    expect(action.type).toBe(visibilityFilterAction.SET_VISIBILITY_FILTER);
    expect(action.filter).toBe(visibilityFilterAction.filters.SHOW_ACTIVE);
  });
  it('should toogle value',()=>{
    const action = visibilityFilterAction.setVisibilityFilter(visibilityFilterAction.filters.SHOW_ALL);
    expect(action.type).toBeDefined();
    expect(action.filter).toBeDefined();

    expect(typeof action.type).toBe('string');
    expect(typeof action.filter).toBe('string');

    expect(action.type).toBe(visibilityFilterAction.SET_VISIBILITY_FILTER);
    expect(action.filter).toBe(visibilityFilterAction.filters.SHOW_ALL);
  });
});
