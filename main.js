import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, browserHistory, IndexRoute, Redirect} from 'react-router';
import App from './app';
import {NotFound, Home, ShowTask, EmptyList, EditTask} from './app/components';
import store from './app/store';
import {Provider} from 'react-redux';

ReactDOM.render((
    <Provider store={store}>
        <Router history={browserHistory}>
            <Redirect from="/" to="categories"/>
            <Route path="/" component={App}>
                <Route path='/categories' component={Home}>
                    <IndexRoute component={EmptyList} type='tasks' reason="start"/>
                    <Route path="/categories/:category_title&:category_id(?:searchText)" component={ShowTask}></Route>
                </Route>
                <Route path="/categories/:category_title&:category_id/:task_title&:task_id" component={EditTask}></Route>
            </Route>
            <Route path="*" component={NotFound}></Route>
        </Router>
    </Provider>
), document.getElementById('app'));
