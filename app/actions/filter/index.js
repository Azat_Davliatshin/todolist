export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';
export const filters = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
}
export function setVisibilityFilter(filter = filters.SHOW_ACTIVE) {
    return {type: SET_VISIBILITY_FILTER, filter}
}
