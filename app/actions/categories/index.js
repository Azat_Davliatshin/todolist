export const ADD_CATEGORY = 'ADD_CATEGORY';
export const EDIT_CATEGORY = 'EDIT_CATEGORY';
export const DELETE_CATEGORY = 'DELETE_CATEGORY';
export const EXTEND_CATEGORY = 'EXTEND_CATEGORY';
export const EXPAND_CATEGORY = 'EXPAND_CATEGORY';
export const TOOGLE_CATEGORY = "TOOGLE_CATEGORY";
export const MAKE_INVISIBLE = "MAKE_INVISIBLE";

export const ADD_TASK = "ADD_TASK";
export const TOOGLE_TASK = "TOOGLE_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const RENAME_TASK = "RENAME_TASK";
export const ADD_TASK_DESCRIPTION = "ADD_TASK_DESCRIPTION";

export const ADD_TO_HISTORY = "ADD_TO_HISTORY";
export const UNDO = "UNDO";
export const REDO = "REDO";

export function addCategory(text = "Category") {
    return {type: ADD_CATEGORY, text}
}
export function deleteCategory(categoryId) {
    return {type: DELETE_CATEGORY, categoryId}
}
export function extendCategory(categoryId) {
    return {type: EXTEND_CATEGORY, categoryId}
}
export function expandCategory(categoryId) {
    return {type: EXPAND_CATEGORY, categoryId}
}
export function editCategory(text, categoryId) {
    return {type: EDIT_CATEGORY, text, categoryId}
}

export function toogleCategory(categoryId, checked) {
    return {type: TOOGLE_CATEGORY, categoryId, checked}
}
export function makeInvisible(categoryId) {
    return {type: MAKE_INVISIBLE, categoryId}
}
export function addTask(text = "To Do Item", categoryId, task) {
    return {type: ADD_TASK, text, categoryId, task}
}
export function toogleTask(categoryId, taskId, checked) {
    return {type: TOOGLE_TASK, categoryId, taskId, checked}
}

export function deleteTask(categoryId, taskId) {
    return {type: DELETE_TASK, categoryId, taskId}
}

export function renameTask(categoryId, taskId, text) {
    return {type: RENAME_TASK, categoryId, taskId, text}
}

export function addTaskDescription(categoryId, taskId, text) {
    return {type: ADD_TASK_DESCRIPTION, categoryId, taskId, text}
}

export function addToHistory() {
    return {type: ADD_TO_HISTORY}
}

export function undo() {
    return {type: UNDO}
}
export function redo() {
    return {type: REDO}
}
