import {categoryAction} from '../../actions';

const initialState = {
    past: [],
    present: JSON.parse(localStorage.getItem('todolists')) || [],
    future: []
};
export default function categories(state = initialState, action) {
    const categoryTemplate = (id, title, level = 0, children = [], isShownChildren = true, completed = true, tasks = [], visible = true) => ({
        id,
        title,
        level,
        children,
        isShownChildren,
        completed, //empty category is completed,
        tasks,
        visible
    });
    let path;
    let current;
    switch (action.type) {
        case categoryAction.ADD_CATEGORY:
            const id = state.present.length
                ? `${parseInt(state.present[0].id) + 1}`
                : `0`;
            return Object.assign({}, state, {
                present: [
                    categoryTemplate(id, action.text),
                    ...state.present
                ]
            });
            break;
        case categoryAction.DELETE_CATEGORY:
            if (action.categoryId.indexOf('_') === -1) {
                return Object.assign({}, state, {
                    present: state.present.filter((category) => category.id !== action.categoryId)
                });
            }
            path = getPathToElement(true);
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        Object.assign(node, {
                            children: node.children.filter((deletedCategory) => deletedCategory.id !== action.categoryId)
                        });
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.EXTEND_CATEGORY:
            path = getPathToElement();
            const ids = action.categoryId.split('_');
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        node.visible = true;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                            node.visible = true;
                        }
                        let uniqueId;
                        if (node.children.length) {
                            const lastIDArray = node.children[0].id.split('_');
                            uniqueId = +lastIDArray[lastIDArray.length - 1] + 1;
                        } else {
                            uniqueId = 0;
                        }
                        const id = [
                            ...ids,
                            uniqueId
                        ].join('_');
                        const title = `sub-${node.title}`;
                        const level = node.level + 1;
                        node.children = [
                            categoryTemplate(id, title, level),
                            ...node.children
                        ];
                        return category2;
                    }
                    return category;
                }).sort((prev, next) => {
                    return next.visible - prev.visible;
                })
            });
            break;
        case categoryAction.EXPAND_CATEGORY:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        Object.assign(node, {
                            isShownChildren: !node.isShownChildren
                        });
                        return category2;
                    }
                    return category;
                })
            });
            break;

        case categoryAction.EDIT_CATEGORY:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        Object.assign(node, {title: action.text});
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.TOOGLE_CATEGORY:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        if (action.checked) {
                            Object.assign(node, {completed: action.checked});
                        } else {
                            Object.assign(node, {
                                completed: node.tasks.every((task) => task.completed)
                            })
                        }
                        return category2;
                    }
                    return category;
                })
            });
        case categoryAction.MAKE_INVISIBLE:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        category2.visible = false;
                        makeInvisible(category2);
                        return category2;
                    }
                    return category;
                }).sort((prev, next) => {
                    return next.visible - prev.visible;
                })
            });
        case categoryAction.ADD_TASK:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        const addedTask = action.task || {
                            id: node.tasks.length
                                ? `${parseInt(node.tasks[0].id) + 1}`
                                : `0`,
                            title: action.text,
                            completed: false,
                            description: ""
                        };
                        Object.assign(node, {
                            tasks: [
                                addedTask, ...node.tasks
                            ]
                        });
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.TOOGLE_TASK:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        node.tasks = node.tasks.map((task) => {
                            if (task.id === action.taskId) {
                                return Object.assign(task, {completed: action.checked});
                            }
                            return task;
                        });
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.DELETE_TASK:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        node.tasks = node.tasks.filter((task) => task.id !== action.taskId);
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.RENAME_TASK:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        node.tasks = node.tasks.map((task) => {
                            if (task.id === action.taskId) {
                                return Object.assign(task, {title: action.text});
                            }
                            return task;
                        });
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.ADD_TASK_DESCRIPTION:
            path = getPathToElement();
            return Object.assign({}, state, {
                present: state.present.map((category, index) => {
                    if (index === path[0]) {
                        const category2 = Object.assign({}, category);
                        let node = category2;
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        node.tasks = node.tasks.map((task) => {
                            if (task.id === action.taskId) {
                                return Object.assign(task, {description: action.text});
                            }
                            return task;
                        });
                        return category2;
                    }
                    return category;
                })
            });
            break;
        case categoryAction.ADD_TO_HISTORY:
            current = Object.assign({}, state);
            current.past = [
                ...current.past,
                JSON.parse(JSON.stringify(state.present))
            ];
            return current;
        case categoryAction.UNDO:
            current = Object.assign({}, state);
            let last = current.past.pop();
            current.present = last;
            current.future = [
                JSON.parse(JSON.stringify(state.present)),
                ...current.future
            ];
            current.present = last;
            return current;
        case categoryAction.REDO:
            current = Object.assign({}, state);
            let first = current.future.shift();
            current.present = first;
            current.past = [
                ...current.past,
                JSON.parse(JSON.stringify(state.present))
            ];
            return current;
        default:
            return state;
    }
    function makeInvisible(category2) {
        category2.children.forEach((category, index, array) => {
            Object.assign(array[index], {visible: false});
            if (category.children.length) {
                makeInvisible(category)
            }
        });
    }
    function getPathToElement(findParent = false) {
        const ids = action.categoryId.split('_');
        let searchScope = state.present;
        let path = [];
        let particalId = ids[0];
        for (let i = 0, lenIds = ids.length - findParent; i < lenIds; i++) {
            particalId = i === 0
                ? ids[i]
                : particalId + `_${ids[i]}`;
            for (let j = 0, lenScope = searchScope.length; j < lenScope; j++) {
                if (searchScope[j].id === particalId) {
                    searchScope = searchScope[j].children;
                    path.push(j);
                    if (i === lenIds - 1) {
                        return path;
                    }
                    break;
                }
            }
        }
    }
}
