import {taskFilterAction} from '../../actions';

function taskFilterText(state = "", action) {
    switch (action.type) {
        case taskFilterAction.SET_FILTER_TEXT:
            return action.text
        default:
            return state
    }
}
export default taskFilterText;
