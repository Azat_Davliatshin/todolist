import {visibilityFilterAction} from '../../actions';

function visibilityFilter(state = visibilityFilterAction.filters.SHOW_ACTIVE, action) {
    switch (action.type) {
        case visibilityFilterAction.SET_VISIBILITY_FILTER:
            return action.filter
        default:
            return state
    }
}
export default visibilityFilter;
