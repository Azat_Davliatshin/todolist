import React, {PropTypes} from 'react'
import {Title} from '../';

const EmptyList = (props) => {
    const messages = {
        tasks: {
            empty: `There is no tasks in '${props.categoryName}' gategory yet!`,
            start: "Select a category to display and edit tasks",
            NotFound: `There is no tasks in '${props.categoryName}' satisfying to search criteries`
        },
        categories: {
            empty: "Create at least one category to see list",
            start: "Select a category to display and edit tasks",
            emptySearch: ""
        }
    };
    const type = props.type || props.route.type;
    const reason = props.reason || props.route.reason;
    return (
        <div className={'component-empty-list ' + type}>
            <Title type="empty">{messages[type][reason]}</Title>
        </div>
    );
};

export default EmptyList;
