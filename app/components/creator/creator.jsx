import React, {PropTypes} from 'react';
import {Input, Button} from '../';
import {categoryAction} from '../../actions';
import store from '../../store';

class Creator extends React.Component {

    constructor(props) {
        super(props);
        this.addComponent = this.addComponent.bind(this);
    }
    toogleClass(event) {
        event.preventDefault();
        const searchBox = event.currentTarget;
        searchBox.classList.toggle('focus');
    }
    addComponent() {
        store.dispatch(categoryAction.addToHistory());
        const inputElement = this.refs.title.refs.input;
        store.dispatch(categoryAction[`add${this.props.type}`](inputElement.value.trim() || undefined, this.props.id));
        if (this.props.type === 'Task') {
            store.dispatch(categoryAction.toogleCategory(this.props.id, false));
        }
        inputElement.value = "";
        inputElement.focus();
    }
    render() {
        return (
            <div className='component-creator' onFocus={this.toogleClass} onBlur={this.toogleClass}>
                <Input placeholder={this.props.placeholder} onEnterDown= {()=>{this.addComponent()}} type='text' ref='title'>
                    <Button onClick={this.addComponent}>Add</Button>
                </Input>
            </div>
        )
    }
}
Creator.propTypes = {
    type: PropTypes.oneOf(['Category', 'Task']).isRequired
};
export default Creator
