import React, {PropTypes} from 'react'

const Icon = (props) => <i className={props.className + props.icon}></i>

Icon.defaultProps = {
    className: "icon icon-"
};
Icon.propTypes = {
    icon: PropTypes.string.isRequired
};
export default Icon;
