import React, {PropTypes} from 'react';
import {Title, Icon, Button} from '../';
import store from '../../store';
import {categoryAction} from '../../actions';

class Category extends React.Component {

    expandCategory(event, id) {
        event.preventDefault();
        store.dispatch(categoryAction.addToHistory());
        store.dispatch(categoryAction.expandCategory(id));
    }
    render() {
        const borderRadius = `${this.props.settings.level * 2}px`,
            marginLeft = `${ !this.props.settings.children.length
                ? 23
                : 0}px`,
            width = `${ (100 - this.props.settings.level * 5)}%`;
        return (
            <div className={`component-category ${this.props.settings.visible ? "visible" : "invisible"}`} style={{
                borderRadius,
                width
            }}>
                <div className="name" style ={{
                    marginLeft
                }}>
                    {!!this.props.settings.children.length && <Button onClick={(event) => this.expandCategory(event, this.props.settings.id)}>
                        <Icon icon={this.props.settings.isShownChildren
                            ? "toogle-down"
                            : "toogle-right"}></Icon>
                    </Button>}
                    <Title type='category'>{this.props.settings.title}</Title>
                </div>
                <div className="control">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Category;
