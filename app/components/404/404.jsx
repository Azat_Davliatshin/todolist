import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import {Icon} from '../';

class NotFound extends React.Component {
    render() {
        return (
            <div style={{"height": "100%"}}>
                <Link to="/">
                    BACK TO APP
                </Link>
                <div className="component-not-found">
                    <div className="image"></div>
                    <div>
                        <h1>404 - NOT FOUND :(</h1>
                        <p>We take your typed URL, try to go to this link, almost there...</p>
                        <p>aaaand...it's gone! Pfff! This URL doesn't exist anymore!</p>
                        <p>You can <Link to="/">go back to application</Link> and try again!</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default NotFound;
