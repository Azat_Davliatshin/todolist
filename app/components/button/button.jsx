import React, {PropTypes} from 'react'

const Button = (props) => <button onClick={props.onClick} className={"component-button " + (props.className || "")}>{props.children}</button>

export default Button;
