import React, {PropTypes} from 'react'

const Title = (props) => <div className={"title-" + props.type}>{props.children}</div>

Title.propTypes = {
    type: PropTypes.string.isRequired
}

export default Title;
