import React, {PropTypes} from 'react'
import {Input, Icon, Button} from '../../../';
import store from '../../../../store';
import {visibilityFilterAction, taskFilterAction} from '../../../../actions';
import {browserHistory} from 'react-router';

class Filter extends React.Component {

    constructor() {
        super();
        this.setFilter = this.setFilter.bind(this);
        this.setTaskFilter = this.setTaskFilter.bind(this);
        this.clearTaskFilter = this.clearTaskFilter.bind(this);

    }

    toogleClass(event) {
        event.preventDefault();
        const searchBox = event.currentTarget;
        searchBox.classList.toggle('focus');
    }

    setFilter(value) {
        const newFilterValue = value
            ? visibilityFilterAction.filters.SHOW_ALL
            : visibilityFilterAction.filters.SHOW_ACTIVE;
        store.dispatch(visibilityFilterAction.setVisibilityFilter(newFilterValue));
    }

    setTaskFilter(value) {
        store.dispatch(taskFilterAction.setFilterText(value));
        browserHistory.push(`${this.props.currentPath}?${value}`);
    }

    clearTaskFilter() {
        store.dispatch(taskFilterAction.setFilterText(""));
        this.refs.search.refs.input.value = "";
        this.refs.search.refs.input.focus();
        browserHistory.push(`${this.props.currentPath}`);
    }

    render() {
        return (
            <div className="component-filter">
                <Input type='checkbox' onChange={this.setFilter}>Show done</Input>
                <span className="component-filter-search" onFocus={this.toogleClass} onBlur={this.toogleClass}>
                    <Input placeholder="Search" onChange={this.setTaskFilter} ref="search">
                        <Button onClick={this.clearTaskFilter}>
                            <Icon icon='cross'></Icon>
                        </Button>
                    </Input>
                </span>
            </div>
        );
    }
}

export default Filter;
