import React, {PropTypes} from 'react'
import {Title, Input} from '../../';
import Filter from './filter';

class Header extends React.Component {
    render() {
        return (
            <header className='component-header'>
                <Title type="header">To-Do-List</Title>
                <Filter currentPath={this.props.currentPath}></Filter>
            </header>
        )
    }
}

export default Header;
