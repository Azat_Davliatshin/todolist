import React, {PropTypes} from 'react';
import store from '../../../store';
class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        const categories = store.getState().categories.present;
        const completed = categories.filter((category) => category.completed); //only top level
        this.state = {
            progress: (completed.length / categories.length) * 100
        };
    }
    componentWillMount() {
        this.unsubscribe = store.subscribe(() => {
            const categories = store.getState().categories.present;
            const completed = categories.filter((category) => category.completed);

            this.setState({
                progress: categories.length
                    ? (completed.length / categories.length) * 100
                    : 100
            });
        });
    }
    componentWillUnmount() {
        this.unsubscribe();
    }
    render() {
        return (
            <div className="uui-progress-bar">
                <div className="uui-progress" style={{
                    width: this.state.progress + '%'
                }} title={`${this.state.progress.toFixed(3)}%`}></div>
            </div>
        )
    }
}

export default ProgressBar;
