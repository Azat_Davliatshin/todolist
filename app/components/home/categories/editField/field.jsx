import React, {PropTypes} from 'react';
import {Input, Button, Icon} from '../../../';
import store from '../../../../store';
import {categoryAction} from '../../../../actions';
import {browserHistory} from 'react-router';

class EditField extends React.Component {
    constructor(props) {
        super(props);
    }
    toogleClass(event) {
        event.preventDefault();
        const searchBox = event.currentTarget;
        searchBox.classList.toggle('focus');
    }
    componentDidMount() {
        this.refs.field.refs.input.focus();
    }
    onBlur(event) {
        event.preventDefault();
        this.toogleClass(event);
    }
    rename(event) {
        const newName = this.refs.field.refs.input.value;
        const id = this.props.id;
        store.dispatch(categoryAction.addToHistory());
        store.dispatch(categoryAction.editCategory(newName, id));

        if (this.props.isSelectedCategory) {
            browserHistory.push(`/categories/${newName}&${id}`);
        }
    }
    render() {
        return (
            <div className="edit-category-field" onFocus={(event) => this.toogleClass(event)} onBlur={(event) => this.onBlur(event)}>
                <Input placeholder="Enter category title" ref='field' value={this.props.value} onEnterDown={() => {
                    this.rename()
                }}>
                    <div className='controls'>
                        <Button onClick={(event) => {
                            event.preventDefault();
                            this.rename()
                        }}>
                            <Icon icon='check'></Icon>
                        </Button>
                        <Button onClick={(event) => {
                            event.preventDefault();
                            store.dispatch({type: 'DO_NOTHING'});
                        }}>
                            <Icon icon='cross'></Icon>
                        </Button>
                    </div>
                </Input>
            </div>
        )
    }
}

export default EditField;
