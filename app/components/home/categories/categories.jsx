import React, {PropTypes} from 'react';
import {Link, browserHistory} from 'react-router';
import {Creator} from '../../';
import {Category, Button, Icon, EmptyList} from '../../';
import store from '../../../store';
import {categoryAction} from '../../../actions';
import EditField from './editField';

class Categories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: this.wrap(this.plainCategories(store.getState().categories.present))
        };
    }
    componentWillMount() {
        this.unsubscribe = store.subscribe(() => {
            this.setState({
                categories: this.wrap(this.plainCategories(store.getState().categories.present))
            })
        });
    }
    componentWillUnmount() {
        this.unsubscribe();
    }
    deleteCategory(event, id, currentPath) {
        event.preventDefault();
        store.dispatch(categoryAction.addToHistory());
        store.dispatch(categoryAction.deleteCategory(id));

    }
    extendCategory(event, category) {
        event.preventDefault();
        if (category.level < 4) {
            store.dispatch(categoryAction.addToHistory());
            store.dispatch(categoryAction.extendCategory(category.id));
        }

    }

    editCategory(event, edtedCategory, currentPath) {
        event.preventDefault();
        const path = currentPath.split('&');
        const isSelectedCategory = (path[path.length - 1] === edtedCategory.id)
        this.setState({
            categories: this.state.categories.map((category) => {
                if (category.key === edtedCategory.id) {
                    return <EditField key={edtedCategory.id} value={edtedCategory.title} id={edtedCategory.id} isSelectedCategory={isSelectedCategory}></EditField>
                }
                return category;
            })
        });
    }
    plainCategories(categories) {
        const result = [];
        categories.forEach((category) => {
            result.push(category);
            if (category.children.length && category.isShownChildren) {
                const children = this.plainCategories(category.children);
                result.push(...children);
            }
        });
        return result;
    }

    wrap(categories) {
        return categories.map((category) => {
            const pathTo = `/categories/${category.title}&${category.id}`;
            const isDisabled = () => {};
            return <Link to={pathTo} key={category.id} activeClassName="selected" className={category.visible ? '' :'disabled-link'}>
                <Category settings={category}>
                    <Button onClick={(event) => this.editCategory(event, category, this.props.currentPath)}>
                        <Icon icon="edit"></Icon>
                    </Button>
                    <Button onClick={(event) => this.deleteCategory(event, category.id)}>
                        <Icon icon="delete"></Icon>
                    </Button>
                    <Button className={category.level === 4
                        ? 'disabled'
                        : ''} onClick={(event) => {
                        this.extendCategory(event, category)
                    }}>
                        <Icon icon="add"></Icon>
                    </Button>

                </Category>
            </Link>
        }).filter((category) => !!category)
    }

    render() {
        return (
            <div className="component-category-overview">
                <Creator placeholder="Enter category title" type='Category'></Creator>
                <div className="categories-wrapper">
                    {this.state.categories.length
                        ? this.state.categories
                        : <EmptyList type='categories' reason='empty'></EmptyList>}
                </div>
                {this.props.children}
            </div>
        )
    }
}

export default Categories;
