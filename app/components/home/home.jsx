import React, {PropTypes} from 'react'
import {Header, ProgressBar, Categories} from './';
import {Creator, Button, LocationControl} from '../';

import store from '../../store';
import {categoryAction} from '../../actions';

class MainWorkflow extends React.Component {
    render() {
        return (
            <div className="component-main-workflow">
                <Header currentPath={this.props.location.pathname}></Header>
                <ProgressBar></ProgressBar>
                <LocationControl currentPath={this.props.location.pathname}></LocationControl>
                <div className="main-panel">
                    <Categories currentPath={this.props.location.pathname}></Categories>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default MainWorkflow;
/*
<div className="main-panel">
  <Categories></Categories>
  {this.props.children}
</div>*/
