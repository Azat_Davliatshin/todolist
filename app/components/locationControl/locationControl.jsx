import React, {PropTypes} from 'react';
import {Button} from '../';
import {Link} from 'react-router';
import store from '../../store';
import {categoryAction} from '../../actions';

class LocationControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isUndoAble: store.getState().categories.past.length > 0,
            isRedoAble: store.getState().categories.future.length > 0
        };
    }
    componentWillMount() {
        this.unsubscribe = store.subscribe(() => {
            this.setState({
                isUndoAble: store.getState().categories.past.length > 0,
                isRedoAble: store.getState().categories.future.length > 0
            });
        });
    }
    componentWillUnmount() {
        this.unsubscribe();
    }
    undo() {
        if (this.state.isUndoAble) {
            store.dispatch(categoryAction.undo());
        }
    }
    redo() {
        if (this.state.isRedoAble) {
            store.dispatch(categoryAction.redo());
        }
    }

    render() {
        const path = this.props.currentPath.split('/').filter((a) => !!a);
        const isUndoAble = store.getState().categories.past.length > 0;
        return (
            <div className="location-control">
                <ol className="uui-breadcrumbs">
                    <li className={path.length === 1
                        ? 'active'
                        : ''}>
                        <Link to={`/${path[0]}`}>/{path[0]}</Link>
                    </li>
                    {path.length > 1
                        ? <li className="active">
                                <Link to={`${this.props.currentPath}`}>categoryTitle: {path[1].split('&')[0]}, categoryId: {path[1].split('&')[1]}</Link>
                            </li>
                        : null}
                </ol>
                <div>
                    <Button className={!this.state.isUndoAble
                        ? 'disabled'
                        : ''} onClick={this.undo.bind(this)}>Undo</Button>
                    <Button className={!this.state.isRedoAble
                        ? 'disabled'
                        : ''} onClick={this.redo.bind(this)}>Redo</Button>
                </div>
            </div>
        );
    }
}

export default LocationControl;
