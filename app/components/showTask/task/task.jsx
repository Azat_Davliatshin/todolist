import React, {PropTypes} from 'react';
import {Button, Icon, Input} from '../../';
import {Link} from 'react-router';
import store from '../../../store';
import {categoryAction} from '../../../actions';
import {browserHistory} from 'react-router';

class Task extends React.Component {
    toogleTask(value) {
        store.dispatch(categoryAction.addToHistory());
        store.dispatch(categoryAction.toogleTask(this.props.params.category_id, this.props.settings.id, value));
        store.dispatch(categoryAction.toogleCategory(this.props.params.category_id));
        const currentCategory = this.getTopLevelCategory(this.props.params.category_id);
        const fullState = this.getFullState(currentCategory);
        if (fullState) {
            store.dispatch(categoryAction.makeInvisible(currentCategory.id));
            browserHistory.push('/categories');
        }

    }

    getFullState(currentCategory) {
        return currentCategory.completed && currentCategory.children.every((child) => {
            return child.completed && this.getFullState(child);
        });
    }
    getTopLevelCategory(id) {
        const categories = store.getState().categories.present;
        const ids = id.split('_')[0];
        for (let i = 0, len = categories.length; i < len; i++) {
            if (categories[i].id === ids) {
                return categories[i];
            }
        }
    }


    render() {
      const pathTo = `/categories/${this.props.params.category_title}&${this.props.params.category_id}/${this.props.settings.title}&${this.props.settings.id}`;
      console.log(this.props.params);
        return (
            <div className="component-task">
                <div className='title'>
                    <Input type='checkbox' onChange={this.toogleTask.bind(this)} value={this.props.settings.completed}>&nbsp;</Input>
                    {this.props.settings.title}
                </div>
                <Link to={pathTo}>
                    <Button>
                        <Icon icon='edit'></Icon>
                    </Button>
                </Link>
            </div>
        )
    }
}

export default Task;
