import React, {PropTypes} from 'react';
import {Creator, EmptyList} from '../';
import {Task} from './';
import store from '../../store';
import {categoryAction, visibilityFilterAction} from '../../actions';
import {browserHistory} from 'react-router';

class ShowTask extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: this.getCategory(this.props.params.category_id).tasks
        };
    }
    getCategory(id) {
        const categories = store.getState().categories.present;
        const ids = id.split('_');
        let searchScope = categories;
        let path = [];
        let particalId = "";
        for (let i = 0, lenIds = ids.length; i < lenIds; i++) {
            particalId = i === 0
                ? ids[0]
                : particalId + `_${ids[i]}`;
            for (let j = 0, lenScope = searchScope.length; j < lenScope; j++) {
                if (searchScope[j].id === particalId) {
                    searchScope = searchScope[j].children;
                    path.push(j);
                    if (i === lenIds - 1) {
                        let node = categories[path[0]];
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        return node;
                    }
                    break;
                }
            }
        }
    }
    componentWillMount() {
        this.unsubscribe = store.subscribe(() => {
            const category = this.getCategory(this.props.params.category_id);
            if (!category) {
                browserHistory.push('/categories');
            } else {
                this.setState({tasks: category.tasks});
            }
        });
    }
    wrap(tasks, params) {
        return tasks.map((task) => {
            if (store.getState().visibilityFilter === visibilityFilterAction.filters.SHOW_ALL || !task.completed) {
                const searchString = store.getState().search;
                if (searchString) {
                    if (task.title.toLowerCase().indexOf(searchString) !== -1) {
                        return <Task key={task.id} params={params} settings={task}></Task>;
                    }
                } else {
                    return <Task key={task.id} params={params} settings={task}></Task>;
                }
            }
        }).filter((task) => task);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            tasks: this.getCategory(nextProps.params.category_id).tasks
        });
    }
    componentWillUnmount() {
        this.unsubscribe();
    }
    render() {
        const wrappedTasks = this.wrap(this.state.tasks, this.props.params);
        return (
            <div className="component-show-task">
                <Creator placeholder="Enter short task description" type="Task" id={this.props.params.category_id}></Creator>
                {this.state.tasks.length
                    ? (wrappedTasks.length
                        ? <div className="task-list">{wrappedTasks}</div>
                        : <EmptyList type='tasks' reason='NotFound' categoryName={this.props.params.category_title}></EmptyList>)
                    : <EmptyList type='tasks' reason='empty' categoryName={this.props.params.category_title}></EmptyList>}
            </div>
        );
    }
}

export default ShowTask;
