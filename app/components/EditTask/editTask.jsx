import React, {PropTypes} from 'react';
import {Title, Button, Input, Category, Icon} from '../';
import {Link, browserHistory} from 'react-router';
import store from '../../store';
import {categoryAction} from '../../actions';

class EditTask extends React.Component {
    constructor(props) {
        super(props);
        const {category_id, task_id} = props.params;
        console.log(this.getCategory(category_id).tasks);
        this.state = {
            categories: this.wrap(this.plainCategories(store.getState().categories.present), props.params),
            task: Object.assign({}, this.getCategory(category_id).tasks.filter((task) => task.id === task_id)[0])
        };
    }

    getCategory(id) {
        const categories = store.getState().categories.present;
        const ids = id.split('_');
        let searchScope = categories;
        let path = [];
        let particalId = "";
        for (let i = 0, lenIds = ids.length; i < lenIds; i++) {
            particalId = i === 0
                ? ids[0]
                : particalId + `_${ids[i]}`;
            for (let j = 0, lenScope = searchScope.length; j < lenScope; j++) {
                if (searchScope[j].id === particalId) {
                    searchScope = searchScope[j].children;
                    path.push(j);
                    if (i === lenIds - 1) {
                        let node = categories[path[0]];
                        for (let k = 1; k < path.length; k++) {
                            node = node.children[path[k]];
                        }
                        return node;
                    }
                    break;
                }
            }
        }
    }
    plainCategories(categories) {
        const result = [];
        categories.forEach((category) => {
            result.push(category);
            if (category.children.length && category.isShownChildren) {
                const children = this.plainCategories(category.children);
                result.push(...children);
            }
        });
        return result;
    }
    wrap(categories, params) {
        return categories.map((category) => {
            return <Category settings={category} key={category.id}>
                {category.id === params.category_id
                    ? null
                    : <Button onClick={(event) => {
                        this.moveTask(event, category)
                    }}>
                        <Icon icon="move"></Icon>
                    </Button>}
            </Category>
        })
    }
    cancel() {
        const {category_title, category_id} = this.props.params;
        const pathTo = `/categories/${category_title}&${category_id}`;
        browserHistory.push(pathTo);
    }
    save(event) {
        event.preventDefault();
        const {category_title, category_id, task_id} = this.props.params;
        const {title, completed, description} = this.state.task;
        store.dispatch(categoryAction.renameTask(category_id, task_id, title));
        store.dispatch(categoryAction.addTaskDescription(category_id, task_id, description));
        store.dispatch(categoryAction.toogleTask(category_id, task_id, completed));
        store.dispatch(categoryAction.toogleCategory(category_id));
        const currentCategory = this.getTopLevelCategory(category_id);
        const fullState = this.getFullState(currentCategory);
        let pathTo;
        if (fullState) {
            store.dispatch(categoryAction.makeInvisible(currentCategory.id));
              pathTo = `/categories`;
        } else {
            pathTo = `/categories/${category_title}&${category_id}`;
        }

        browserHistory.push(pathTo);
    }
    getFullState(currentCategory) {
        return currentCategory.completed && currentCategory.children.every((child) => {
            return child.completed && this.getFullState(child);
        });
    }
    getTopLevelCategory(id) {
        const categories = store.getState().categories.present;
        const ids = id.split('_')[0];
        for (let i = 0, len = categories.length; i < len; i++) {
            if (categories[i].id === ids) {
                return categories[i];
            }
        }
    }
    moveTask(event, srcCategory) {
        event.preventDefault();
        const {title, id} = this.state.task;
        store.dispatch(categoryAction.addTask(title, srcCategory.id, this.getCategory(this.props.params.category_id).tasks.filter((task) => task.id === this.props.params.task_id)[0]));
        store.dispatch(categoryAction.deleteTask(this.props.params.category_id, id));
        const pathTo = `/categories/${srcCategory.title}&${srcCategory.id}/${title}&${id}`;
        browserHistory.push(pathTo);

    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            categories: this.wrap(this.plainCategories(store.getState().categories.present), nextProps.params)
        })
    }
    render() {
        const {title, id, completed, description} = this.state.task;
        const pathTo = `/categories/${title}`;
        return (
            <div className='task-edit-wrapper'>
                <Title type='header'>{this.props.params.task_title}</Title>
                <div className="task-edit">
                    <div className="task-edit-category">
                        {this.state.categories}
                    </div>
                    <div className="task-edit-info">
                        <div className="controls">
                            <Button onClick={(event) => {
                                this.save(event)
                            }}>
                                Save changes
                            </Button>
                            <Button onClick={(event) => {
                                this.cancel(event)
                            }}>
                                Cancel
                            </Button>
                        </div>
                        <Input value={title} type='text' placeholder='Input task title' onChange={(value) => {
                            this.setState({
                                task: Object.assign(this.state.task, {title: value})
                            });
                        }}></Input>
                        <Input type='checkbox' value={completed} onChange={(value) => {
                            this.setState({
                                task: Object.assign(this.state.task, {completed: value})
                            });
                        }}>Done</Input>
                        <div className="field">
                            <textarea placeholder='Description' value={description} onChange={(event) => {
                                event.preventDefault();
                                const value = event.currentTarget.value;
                                this.setState({
                                    task: Object.assign(this.state.task, {description: value})
                                });
                            }}></textarea>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditTask;
