import React, {PropTypes} from 'react'

class Input extends React.Component {
    constructor(props) {
        super(props);
        if (props.type === 'text') {
            this.state = {
                value: ""
            }
        } else {
            this.state = {
                value: false
            }
        }
        this.onChange = this.onChange.bind(this);
    }
    onChange(event) {
        const value = event.currentTarget[(this.props.type === 'text'
                ? 'value'
                : 'checked')];
        this.setState({value});
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    onKeyDown(event) {
        switch (event.key) {
            case 'Escape':
                event.preventDefault();
                event.currentTarget.value = "";
                break;
            case 'Tab':
                event.preventDefault();
                break;
            case 'Enter':
                if (this.props.onEnterDown) {
                    event.preventDefault();
                    this.props.onEnterDown();
                }
                break;
        }
    }
    render() {
        return (
            <label className={'component-input ' + this.props.type}>
                <input type={this.props.type} placeholder={this.props.placeholder} defaultValue={this.props.value} onChange={this.onChange} onKeyDown ={this.onKeyDown.bind(this)} checked={this.props.value}  ref="input"/>
                <span>{this.props.children}</span>
            </label>
        )
    }
}

Input.defaultProps = {
    type: 'text'
}
Input.propTypes = {
    placeholder: PropTypes.string,
    type: React.PropTypes.oneOf(['text', 'checkbox'])
};
export default Input;
