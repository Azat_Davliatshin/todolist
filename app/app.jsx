import React, {PropTypes} from 'react'
import store from './store';

import {categoryAction} from './actions';

import './app.less';

class App extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        this.unsubscribe = store.subscribe(() => {
            localStorage.setItem('todolists', JSON.stringify(store.getState().categories.present));
        });
    }
    componentWillUnmount() {
        this.unsubscribe();
    }
    render() {
        return <div>{this.props.children}</div>
    }
}

export default App;
