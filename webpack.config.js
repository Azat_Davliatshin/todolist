    var path = require("path");
    var HtmlWebpackPlugin = require('html-webpack-plugin');

    module.exports = {
        entry: ["./main.js"],
        output: {
            path: path.resolve(__dirname, "build"),
            publicPath: "/assets/",
            filename: "bundle.js"
        },
        plugins: [
            new HtmlWebpackPlugin({
                title: 'To-Do List',
                filename: 'index.html',
                template: './index.ejs'
            })
        ],
        devServer: {
            port: 3333,
            contentBase: "build/",
            outputPath: path.resolve(__dirname, "build")
        },
        module: {
            loaders: [{
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
            }, {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader"
            }, {
                test: /\.(jpg|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader: 'file-loader'
            }]
        }
    }
